package main

import (
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"time"
	"bufio"
	"context"
	"encoding/csv"
	"flag"
	"strings"
	"sync"
	"fmt"
)

type Candle struct {
	Ticker string
	Timestamp time.Time
	OpeningPrice float64
	MaxPrice float64
	MinPrice float64
	ClosingPrice float64
}

type Trade struct {
	Ticker string
	Price float64
	Amount int
	Timestamp time.Time
}

// helper functions
func getMaxCandlePrice(trade []Trade) float64 {
	maxPriceValue := trade[0].Price

	for _, currentValue := range trade {
		if currentValue.Price > maxPriceValue {
			maxPriceValue = currentValue.Price
		}
	}

	return maxPriceValue
}

func getMinCandlePrice(trade []Trade) float64 {
	minPriceValue := trade[0].Price

	for _, currentValue := range trade {
		if currentValue.Price < minPriceValue {
			minPriceValue = currentValue.Price
		}
	}

	return minPriceValue
}

func candleFormat(candle Candle) string {
	timestamp := candle.Timestamp.Format(time.RFC3339)
	openingPrice := strconv.FormatFloat(candle.OpeningPrice, 'f', -1, 64)
	maxPrice := strconv.FormatFloat(candle.MaxPrice, 'f', -1, 64)
	minPrice := strconv.FormatFloat(candle.MinPrice, 'f', -1, 64)
	closingPrice := strconv.FormatFloat(candle.ClosingPrice, 'f', -1, 64)

	resultString := fmt.Sprintf("%s,%s,%s,%s,%s,%s\n", candle.Ticker, timestamp, openingPrice, maxPrice, minPrice, closingPrice)

	return resultString
}

// STAGE 1: read/write functions
func readFileConcurrently(filename string, cntx context.Context) <-chan Trade {
	resultFile := make(chan Trade)
	tradesCSV, err := os.Open(filename)

	if err != nil {
		log.Fatal("can`t open file: ", err)
	}

	reader := csv.NewReader(bufio.NewReader(tradesCSV))

	go func(file *os.File) {
		defer file.Close()
		defer close(resultFile)

		for {
			line, err := reader.Read()

			if err == io.EOF {
				return
			}

			timestamp := line[3]

			tsDate := strings.Split(timestamp, " ")[0]
			tsTime := strings.Split(timestamp, " ")[1]

			timestampData, err := time.Parse(time.RFC3339, tsDate + "T" + tsTime + "Z")

			if err != nil {
				fmt.Println("Couldn't parse time: ", err)
			}

			price, err := strconv.ParseFloat(line[1], 64)

			if err != nil {
				fmt.Println("Couldn't parse price: ", err)
			}

			count, err := strconv.Atoi(line[2])

			if err != nil {
				fmt.Println("Couldn't parse amount: ", err)
			}

			trade := Trade{
				Ticker: line[0],
				Price: price,
				Amount: count,
				Timestamp: timestampData,
			}

			select {
			case resultFile <- trade:
			case <-cntx.Done():
				fmt.Println("Context terminated due to timeout")
				return
			}
		}

	}(tradesCSV)

	return resultFile
}

func writeCSV(channelData <-chan []Candle, filename string) {
	file, err := os.Create(filename)

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	for candles := range channelData {
		if len(candles) == 0 {
			continue
		}

		for _, candle := range candles {
			str := candleFormat(candle)

			if _, err := file.WriteString(str); err != nil {
				log.Fatal(err)
			}
		}
	}
}

func writeResult(candles5minChan, candles30minChan, candles240minChan <-chan []Candle) {
	var wg sync.WaitGroup
	wg.Add(3)

	writeToFile := func(filename string, channelData <-chan []Candle) {
		defer wg.Done()
		writeCSV(channelData, filename)
	}

	go writeToFile("candles_5m.csv", candles5minChan)
	go writeToFile("candles_30m.csv", candles30minChan)
	go writeToFile("candles_240m.csv", candles240minChan)

	wg.Wait()
}

// STAGE 2: candles create functions
func groupTradesByTimestampInterval(tradeChannelData <-chan Trade) (<-chan Trade, <-chan Trade, <-chan Trade) {
	trade5minData := make(chan Trade)
	trade30minData := make(chan Trade)
	trade240minData := make(chan Trade)

	go func(<-chan Trade, <-chan Trade, <-chan Trade) {
		defer close(trade5minData)
		defer close(trade30minData)
		defer close(trade240minData)

		for tradeVal := range tradeChannelData {
			trade5minData <- tradeVal
			trade30minData <- tradeVal
			trade240minData <- tradeVal
		}

	}(trade5minData, trade30minData, trade240minData)

	return trade5minData, trade30minData, trade240minData
}

func computeCandleFromTrade(trades []Trade, timestamp time.Time) Candle {
	candle := Candle{
		Ticker: trades[0].Ticker,
		OpeningPrice: trades[0].Price,
		MaxPrice: getMaxCandlePrice(trades),
		MinPrice: getMinCandlePrice(trades),
		ClosingPrice: trades[len(trades) - 1].Price,
		Timestamp: timestamp,
	}

	return candle
}

func createCandles(tickers map[string][]Trade, start time.Time) []Candle {
	var candle Candle
	var candles []Candle

	for _, tradeVal := range tickers {
		candle = computeCandleFromTrade(tradeVal, start)
		candles = append(candles, candle)
	}

	sort.Slice(candles, func(lhs, rhs int) bool {
		return candles[lhs].MinPrice < candles[rhs].MinPrice
	})

	return candles
}

func createCandleFromTradeWithInterval(tradeDataChannel <-chan Trade, candleDataChannel chan []Candle, interval time.Duration) {
	tickers := make(map[string][]Trade)

	var ts time.Time
	var err error

	for trade := range tradeDataChannel {
		if trade.Timestamp.Hour() < 7 && trade.Timestamp.Hour() >= 3 {

			candles := createCandles(tickers, ts)
			day := strings.Split(trade.Timestamp.String(), " ")[0]
			ts, err = time.Parse(time.RFC3339, day + "T07:00:00Z")

			if err != nil {
				fmt.Println("can`t parse the time: ", err)
			}

			candleDataChannel <- candles
			tickers = map[string][]Trade{}

			continue
		}

		if ts.Add(interval).Before(trade.Timestamp) {
			candles := createCandles(tickers, ts)
			candleDataChannel <- candles
			tickers = map[string][]Trade{}
			ts = ts.Add(interval)
		}

		tickers[trade.Ticker] = append(tickers[trade.Ticker], trade)
	}

	candles := createCandles(tickers, ts)
	candleDataChannel <- candles
}

func getCandlesWithIntervals(tradeChannelData5min, tradeChannelData30min, tradeChannelData240min <-chan Trade) (<-chan []Candle, <-chan []Candle, <-chan []Candle) {
	candles5minChan := make(chan []Candle)
	candles30minChan := make(chan []Candle)
	candles240minChan := make(chan []Candle)

	constructCandle := func(tradeChannelData <-chan Trade, candleChannelData chan []Candle, interval time.Duration) {
		defer close(candleChannelData)
		createCandleFromTradeWithInterval(tradeChannelData, candleChannelData, interval)
	}

	go constructCandle(tradeChannelData5min, candles5minChan, 5 * time.Minute)
	go constructCandle(tradeChannelData30min, candles30minChan, 30 * time.Minute)
	go constructCandle(tradeChannelData240min, candles240minChan, 240 * time.Minute)

	return candles5minChan, candles30minChan, candles240minChan
}

// STAGE 3: construct pipeline
func createPipeline() {
	waitTime := 5 * time.Second
	cntx, finish := context.WithTimeout(context.Background(), waitTime)

	defer finish()

	var filename string

	flag.StringVar(&filename, "file", "", "")
	flag.Parse()

	fileReadingChan := readFileConcurrently(filename, cntx)

	candles5min, candles30min, candles240min := groupTradesByTimestampInterval(fileReadingChan)
	candles5minChan, candles30minChan, candles240minChan := getCandlesWithIntervals(candles5min, candles30min, candles240min)

	writeResult(candles5minChan, candles30minChan, candles240minChan)
}

func main() {
	createPipeline()
}


// var concurrency = 100
//
// func main() {
// 	lines := make(chan string)
//
// 	var wg sync.WaitGroup
// 	wg.Add(concurrency)
//
// 	go func() {
// 		file, err := os.Open("trades.csv")
// 		if err != nil {
// 			log.Fatal(err)
// 		}
//
// 		defer file.Close()
//
// 		scanner := bufio.NewScanner(file)
//
// 		for scanner.Scan() {
// 			lines <- scanner.Text()
// 		}
// 		close(lines)
// 	}()
//
// 	for i := 0; i < concurrency; i++ {
// 		go func(lines chan string) {
// 			defer wg.Done()
// 			for line := range lines {
// 				fmt.Println(line)
// 			}
// 		}(lines)
// 	}
//
// 	wg.Wait()
// }
